package mx.com.amis.catalogclient.dto;

public class ChildCatalogs {
  private String tableName;
  private String urlValuesSubtacatalog;

  public String getTableName() {
    return tableName;
  }

  public void setTableName(String tableName) {
    this.tableName = tableName;
  }

  public String getUrlValuesSubtacatalog() {
    return urlValuesSubtacatalog;
  }

  public void setUrlValuesSubtacatalog(String urlValuesSubtacatalog) {
    this.urlValuesSubtacatalog = urlValuesSubtacatalog;
  }
}
