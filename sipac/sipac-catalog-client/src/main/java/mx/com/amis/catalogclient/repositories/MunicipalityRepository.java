package mx.com.amis.catalogclient.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.com.amis.catalogclient.entities.Municipio;

public interface MunicipalityRepository extends JpaRepository<Municipio, Long> {

}
