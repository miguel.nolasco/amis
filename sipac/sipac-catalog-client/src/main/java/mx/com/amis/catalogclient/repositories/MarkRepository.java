package mx.com.amis.catalogclient.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import mx.com.amis.catalogclient.entities.Marca;

@Repository
public interface MarkRepository extends JpaRepository<Marca, Long> {

  @Query("SELECT mar FROM Marca mar WHERE UPPER(mar.id) = UPPER(?1)")
  public Marca findById(final String nameId);
}
