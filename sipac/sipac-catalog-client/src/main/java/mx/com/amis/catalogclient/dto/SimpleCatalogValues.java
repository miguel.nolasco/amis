package mx.com.amis.catalogclient.dto;

import java.util.List;

public class SimpleCatalogValues {
  private String idReg;
  private String idTable;
  private String id1;
  private String id2;
  private String id3;
  private String id4;
  private String id5;
  private String value1;
  private String value2;
  private String value3;
  private String value4;
  private String value5;
  private List<ChildCatalogs> childCatalogs;

  public String getIdReg() {
    return idReg;
  }

  public void setIdReg(String idReg) {
    this.idReg = idReg;
  }

  public String getIdTable() {
    return idTable;
  }

  public void setIdTable(String idTable) {
    this.idTable = idTable;
  }

  public String getId1() {
    return id1;
  }

  public void setId1(String id1) {
    this.id1 = id1;
  }

  public String getId2() {
    return id2;
  }

  public void setId2(String id2) {
    this.id2 = id2;
  }

  public String getId3() {
    return id3;
  }

  public void setId3(String id3) {
    this.id3 = id3;
  }

  public String getId4() {
    return id4;
  }

  public void setId4(String id4) {
    this.id4 = id4;
  }

  public String getId5() {
    return id5;
  }

  public void setId5(String id5) {
    this.id5 = id5;
  }

  public String getValue1() {
    return value1;
  }

  public void setValue1(String value1) {
    this.value1 = value1;
  }

  public String getValue2() {
    return value2;
  }

  public void setValue2(String value2) {
    this.value2 = value2;
  }

  public String getValue3() {
    return value3;
  }

  public void setValue3(String value3) {
    this.value3 = value3;
  }

  public String getValue4() {
    return value4;
  }

  public void setValue4(String value4) {
    this.value4 = value4;
  }

  public String getValue5() {
    return value5;
  }

  public void setValue5(String value5) {
    this.value5 = value5;
  }

  public List<ChildCatalogs> getChildCatalogs() {
    return childCatalogs;
  }

  public void setChildCatalogs(List<ChildCatalogs> childCatalogs) {
    this.childCatalogs = childCatalogs;
  }

}
