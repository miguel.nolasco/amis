package mx.com.amis.catalogclient.dto;

public class SimpleCatalog {
  private String tableId;
  private String description;
  private String pk;
  private String fk;
  private String fkTable;
  private String valuesUrl;
  
  public String getTableId() {
    return tableId;
  }
  
  public void setTableId(String tableId) {
    this.tableId = tableId;
  }
  
  public String getDescription() {
    return description;
  }
  
  public void setDescription(String description) {
    this.description = description;
  }
  
  public String getPk() {
    return pk;
  }
  
  public void setPk(String pk) {
    this.pk = pk;
  }
  
  public String getFk() {
    return fk;
  }
  
  public void setFk(String fk) {
    this.fk = fk;
  }
  
  public String getFkTable() {
    return fkTable;
  }
  
  public void setFkTable(String fkTable) {
    this.fkTable = fkTable;
  }
  
  public String getValuesUrl() {
    return valuesUrl;
  }
  public void setValuesUrl(String valuesUrl) {
    this.valuesUrl = valuesUrl;
  }
}
