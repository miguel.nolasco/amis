package mx.com.amis.catalogclient.dto;

public class Catalog {
  private String idCatalogGeneral;
  private String idCatalogEstructure;
  private String catalogName;
  private Object uri;

  public String getIdCatalogGeneral() {
    return idCatalogGeneral;
  }

  public void setIdCatalogGeneral(final String idCatalogGeneral) {
    this.idCatalogGeneral = idCatalogGeneral;
  }

  public String getIdCatalogEstructure() {
    return idCatalogEstructure;
  }

  public void setIdCatalogEstructure(final String idCatalogEstructure) {
    this.idCatalogEstructure = idCatalogEstructure;
  }

  public String getCatalogName() {
    return catalogName;
  }

  public void setCatalogName(final String catalogName) {
    this.catalogName = catalogName;
  }

  public Object getUri() {
    return uri;
  }

  public void setUri(Object uri) {
    this.uri = uri;
  }
}
