package mx.com.amis.catalogclient.constants;

public class CodeErrorClient {
  public static final int NONEXISTENT_CATALOG = 1;
  public static final int ERROR_EXTRACTING_CATALOG = 2;
  public static final int ERROR_SAVING_DATABASE = 3;

  public static final int REST_CLIENT_EXCEPTION = 4;
}
