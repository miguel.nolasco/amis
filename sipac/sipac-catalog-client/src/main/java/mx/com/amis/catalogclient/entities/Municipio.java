package mx.com.amis.catalogclient.entities;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "C_MUNICIPIO")
public class Municipio implements UpdatableCatalog {
  @Id
  @Column(name = "PK_municipio", nullable = false)
  private Long id;
  @Column(name = "descripcion", nullable = false)
  private String description;
  @Column(name = "fecha", nullable = false)
  private Date date;
  @Column(name = "activo", nullable = false)
  private boolean active;

  public Municipio(Long id, String description, Date date, boolean active) {
    this.id = id;
    this.description = description;
    this.date = date;
    this.active = active;
  }

  public Municipio() {
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }

}