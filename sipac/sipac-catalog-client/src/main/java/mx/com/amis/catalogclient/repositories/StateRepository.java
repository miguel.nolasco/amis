package mx.com.amis.catalogclient.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.com.amis.catalogclient.entities.Estado;

public interface StateRepository extends JpaRepository<Estado, Long> {

}
