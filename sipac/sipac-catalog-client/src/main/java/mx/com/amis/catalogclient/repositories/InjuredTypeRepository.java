package mx.com.amis.catalogclient.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.com.amis.catalogclient.entities.TipoLesionado;

public interface InjuredTypeRepository extends JpaRepository<TipoLesionado, Long> {

}
