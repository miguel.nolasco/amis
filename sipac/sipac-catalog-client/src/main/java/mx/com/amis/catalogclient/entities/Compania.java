package mx.com.amis.catalogclient.entities;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "C_COMPANIA")
public class Compania {
  @Id
  @Column(name = "PK_compania", nullable = false)
  private Long id;
  @Column(name = "descripcion", nullable = false)
  private String description;
  private String rfc;
  private String alias;
  private boolean factura;
  private boolean stp;
  @Column(name = "fecha", nullable = false)
  private Date date;
  @Column(name = "activo", nullable = false)
  private boolean active;

  public Compania(Long id, String description, String rfc, String alias, boolean factura,
      boolean stp, Date date, boolean active) {
    this.id = id;
    this.description = description;
    this.rfc = rfc;
    this.alias = alias;
    this.factura = factura;
    this.stp = stp;
    this.date = date;
    this.active = active;
  }

  public Compania() {
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getRfc() {
    return rfc;
  }

  public void setRfc(String rfc) {
    this.rfc = rfc;
  }

  public String getAlias() {
    return alias;
  }

  public void setAlias(String alias) {
    this.alias = alias;
  }

  public boolean isFactura() {
    return factura;
  }

  public void setFactura(boolean factura) {
    this.factura = factura;
  }

  public boolean isStp() {
    return stp;
  }

  public void setStp(boolean stp) {
    this.stp = stp;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }
}
