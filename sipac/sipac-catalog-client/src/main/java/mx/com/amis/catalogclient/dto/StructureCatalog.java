package mx.com.amis.catalogclient.dto;

public class StructureCatalog {
  private String idEstructure;
  private String idCatalogGeneral;
  private String idTable;
  private String idTableFather;
  private String idColCascaded;
  private String finalColReference;
  private String finalColDescription;
  private String level;
  private String simpleCatalogUri;
  private String cascaded;
  
  public String getIdEstructure() {
    return idEstructure;
  }
  
  public void setIdEstructure(String idEstructure) {
    this.idEstructure = idEstructure;
  }
  
  public String getIdCatalogGeneral() {
    return idCatalogGeneral;
  }
  
  public void setIdCatalogGeneral(String idCatalogGeneral) {
    this.idCatalogGeneral = idCatalogGeneral;
  }
  
  public String getIdTable() {
    return idTable;
  }
  public void setIdTable(String idTable) {
    this.idTable = idTable;
  }
  
  public String getIdTableFather() {
    return idTableFather;
  }
  
  public void setIdTableFather(String idTableFather) {
    this.idTableFather = idTableFather;
  }
  
  public String getIdColCascaded() {
    return idColCascaded;
  }
  
  public void setIdColCascaded(String idColCascaded) {
    this.idColCascaded = idColCascaded;
  }
  
  public String getFinalColReference() {
    return finalColReference;
  }
  
  public void setFinalColReference(String finalColReference) {
    this.finalColReference = finalColReference;
  }
  
  public String getFinalColDescription() {
    return finalColDescription;
  }
  
  public void setFinalColDescription(String finalColDescription) {
    this.finalColDescription = finalColDescription;
  }
  
  public String getLevel() {
    return level;
  }
  
  public void setLevel(String level) {
    this.level = level;
  }
  
  public String getSimpleCatalogUri() {
    return simpleCatalogUri;
  }
  
  public void setSimpleCatalogUri(String simpleCatalogUri) {
    this.simpleCatalogUri = simpleCatalogUri;
  }
  
  public String getCascaded() {
    return cascaded;
  }
  
  public void setCascaded(String cascaded) {
    this.cascaded = cascaded;
  }
  
  
}
