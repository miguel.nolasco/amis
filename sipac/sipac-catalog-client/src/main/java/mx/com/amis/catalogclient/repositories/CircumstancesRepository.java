package mx.com.amis.catalogclient.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.com.amis.catalogclient.entities.Circunstancia;

public interface CircumstancesRepository extends JpaRepository<Circunstancia, Long> {

}
