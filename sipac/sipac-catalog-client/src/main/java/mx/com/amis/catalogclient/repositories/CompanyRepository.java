package mx.com.amis.catalogclient.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.com.amis.catalogclient.entities.Compania;

public interface CompanyRepository extends JpaRepository<Compania, Long> {

}
