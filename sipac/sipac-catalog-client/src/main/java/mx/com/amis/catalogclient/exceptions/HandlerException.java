package mx.com.amis.catalogclient.exceptions;

public class HandlerException extends Exception {
  private int codeError;
  private String message;

  public HandlerException() {

  }

  public HandlerException(final int codeError, final String message) {
    this.codeError = codeError;
    this.message = message;
  }

  public int getCodeError() {
    return codeError;
  }

  public void setCodeError(int codeError) {
    this.codeError = codeError;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

}
