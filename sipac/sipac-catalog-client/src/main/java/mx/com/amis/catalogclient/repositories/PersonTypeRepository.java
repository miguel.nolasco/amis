package mx.com.amis.catalogclient.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.com.amis.catalogclient.entities.TipoPersona;

public interface PersonTypeRepository extends JpaRepository<TipoPersona, Long> {

}
