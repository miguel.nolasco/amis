package mx.com.amis.catalogclient.entities;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "C_ESTADO")
public class Estado implements UpdatableCatalog {
  @Id
  @Column(name = "PK_estado", nullable = false)
  private Long id;
  @Column(name = "descripcion", nullable = false)
  private String description;
  @Column(name = "fecha", nullable = false)
  private Date date;
  @Column(name = "activo", nullable = false)
  private boolean active;

  public Estado(Long id, String description, Date date, boolean active) {
    this.id = id;
    this.description = description;
    this.date = date;
    this.active = active;
  }

  public Estado() {
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }

}
