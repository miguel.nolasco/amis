package mx.com.amis.catalogclient.constants;

public class CatalogList {
  public static final String TIPO_TRANSPORTE = "TIPO_TRANSPORTE";
  public static final String MARCA = "MARCA";
  public static final String USO_DEL_VEHICULO = "153-USO_DEL_VEHICULO";
  public static final String COBERTURA = "17.02-COBERTURA";
  public static final String TIPO_PERSONA = "TIPO_PERSONA";
  public static final String TIPO_LESIONADO = "TIPO_LESIONADO";
  public static final String LESION = "LESION";
  public static final String COMPANIA = "COMPANIA";
  public static final String COLOR = "COLOR";
  public static final String MODELO = "MODELO";
  public static final String MUNICIPIO = "MUNICIPIO";
  public static final String ESTADO = "ESTADO";
  public static final String CIRCUNSTANCIA = "CIRCUNSTANCIA";

  public static final String TIPO_ORDEN = "TIPO_ORDEN";
  public static final String ORIGEN = "ORIGEN";
}
