package mx.com.amis.catalogclient.constants;

public class StaticValues {
  public static final String URI_CATALOGS = "http://api.seguridad-amis.com:8881/catalog";
  public static final String URI_STRUCTURE_CATALOG = "http://api.seguridad-amis.com:8881/estructureCatalog";
  public static final String URI_SIMPLE_CATALOG = "http://api.seguridad-amis.com:8881/simpleCatalog";
}
