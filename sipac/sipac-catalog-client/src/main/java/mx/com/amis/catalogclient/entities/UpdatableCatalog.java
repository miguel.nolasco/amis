package mx.com.amis.catalogclient.entities;

import java.sql.Date;

public interface UpdatableCatalog {
  public Long getId();

  public void setId(Long id);

  public String getDescription();

  public void setDescription(String description);

  public Date getDate();

  public void setDate(Date date);

  public boolean isActive();

  public void setActive(boolean active);

}
