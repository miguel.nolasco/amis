package mx.com.amis.catalogclient.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "C_TIPO_PERSONA")
public class TipoPersona {
  @Id
  @Column(name = "PK_tipo_persona", nullable = false)
  private Long id;
  @Column(name = "descripcion", nullable = false)
  private String description;

  public TipoPersona(Long id, String description) {
    this.id = id;
    this.description = description;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
