package mx.com.amis.catalogclient.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mx.com.amis.catalogclient.entities.UsoVehiculo;

@Repository
public interface VehicleUseRepository extends JpaRepository<UsoVehiculo, Long> {

}
