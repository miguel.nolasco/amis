package mx.com.amis.catalogclient.clients;

import java.lang.reflect.InvocationTargetException;
import java.sql.Date;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import mx.com.amis.catalogclient.constants.CatalogList;
import mx.com.amis.catalogclient.constants.CodeErrorClient;
import mx.com.amis.catalogclient.constants.StaticValues;
import mx.com.amis.catalogclient.dto.SimpleCatalog;
import mx.com.amis.catalogclient.dto.SimpleCatalogValues;
import mx.com.amis.catalogclient.entities.Marca;
import mx.com.amis.catalogclient.entities.TipoTransporte;
import mx.com.amis.catalogclient.entities.UpdatableCatalog;
import mx.com.amis.catalogclient.entities.UsoVehiculo;
import mx.com.amis.catalogclient.exceptions.HandlerException;
import mx.com.amis.catalogclient.repositories.MarkRepository;
import mx.com.amis.catalogclient.repositories.TransportTypeRepository;
import mx.com.amis.catalogclient.repositories.VehicleUseRepository;

@Service
public class CatalogClient {
  @Autowired
  private MarkRepository marcaRepository;
  @Autowired
  private TransportTypeRepository transportTypeRepository;
  @Autowired
  private VehicleUseRepository vehicleUseRepository;

  private static final Logger LOGGER = LoggerFactory.getLogger(CatalogClient.class);
  private static final RestTemplate restTemplate = new RestTemplate();

  /**
   * Realiza solicitud al catalogo REST para obtener los valores del catalogo.
   * deseado.
   * 
   * @param catalogName
   * @return
   * @throws HandlerException
   */
  public List<SimpleCatalogValues> getCatalogValues(final String catalogName)
      throws HandlerException {
    SimpleCatalog simpleCatalog = getSimpleCatalog(catalogName);

    if (simpleCatalog == null) {
      throw new HandlerException(CodeErrorClient.NONEXISTENT_CATALOG, "No exite catalogo");
    }

    ResponseEntity<List<SimpleCatalogValues>> simpleCatalogValues = null;

    try {
      String uriValues = simpleCatalog.getValuesUrl();
      ParameterizedTypeReference<List<SimpleCatalogValues>> typeRef = new ParameterizedTypeReference<List<SimpleCatalogValues>>() {
      };

      simpleCatalogValues = restTemplate.exchange(uriValues, HttpMethod.GET, null, typeRef);
    } catch (RestClientException excep) {
      throw new HandlerException(CodeErrorClient.REST_CLIENT_EXCEPTION, excep.toString());
    }

    return simpleCatalogValues.getBody();
  }

  /**
   * Realiza solicitud al catalogo REST y retorna la definicion del catalogo
   * deseado.
   * 
   * @param idTable
   *          Nombre del catalogo.
   * @return
   * @throws HandlerException
   */
  public SimpleCatalog getSimpleCatalog(final String idTable) throws HandlerException {

    LOGGER.info("Obteniendo SimpleCatalog...");

    ResponseEntity<SimpleCatalog> simpleCatalogs = null;
    try {

      ParameterizedTypeReference<SimpleCatalog> typeRef = new ParameterizedTypeReference<SimpleCatalog>() {
      };

      String uri = StaticValues.URI_SIMPLE_CATALOG + "/" + idTable;
      simpleCatalogs = restTemplate.exchange(uri, HttpMethod.GET, null, typeRef);
    } catch (RestClientException excep) {
      throw new HandlerException(CodeErrorClient.REST_CLIENT_EXCEPTION, excep.toString());
    }

    LOGGER.info("...SimpleCatalog OK");
    return simpleCatalogs.getBody();
  }

  /**
   * Extrae la informacion de los catalogos externos y los almacena en objetos que
   * modelan las tablas en base de datos.
   * 
   * @param catalogName
   *          Nombre del catalogo
   * @param clazz
   *          Definicion de la clase Entity que modela la tabla en BD
   * @return
   * @throws HandlerException
   */
  public List<Object> getEntitiesFromCatalogExt(final String catalogName, Class<?> clazz)
      throws HandlerException {
    List<Object> response = new ArrayList<>();
    List<SimpleCatalogValues> simpleCatalogValues = null;
    LocalDate now = LocalDate.now();
    try {
      simpleCatalogValues = getCatalogValues(catalogName);
    } catch (Exception excep) {
      LOGGER.info(excep.getMessage());
      throw new HandlerException(CodeErrorClient.ERROR_EXTRACTING_CATALOG,
          "Error al extraer catalogo");
    }
    for (SimpleCatalogValues value : simpleCatalogValues) {
      Object object = null;
      try {
        object = Class.forName(clazz.getName()).getConstructor().newInstance(new Object[] {});
      } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
          | InvocationTargetException | NoSuchMethodException | SecurityException
          | ClassNotFoundException e) {
        LOGGER.info(e.getMessage());
        throw new HandlerException(CodeErrorClient.ERROR_EXTRACTING_CATALOG,
            "Error al extraer catalogo");
      }

      UpdatableCatalog register = (UpdatableCatalog) object;

      try {
        register.setId(new Long(value.getId1()));
        register.setDescription(value.getValue1());
        register.setDate(Date.valueOf(now));
        register.setActive(true);
        response.add(register);
      } catch (Exception excep) {
        LOGGER.info("Error al extraer dato del catalogo externo");
        LOGGER.info(excep.getMessage());
      }
    }
    return response;
  }

  /**
   * Actualiza los catalogos de Base de datos.
   * 
   * @throws HandlerException
   */
  public void updateCatalogs() throws HandlerException {
    try {
      final LocalTime init = LocalTime.now();

      List<Object> marcas = getEntitiesFromCatalogExt(CatalogList.MARCA, Marca.class);
      LOGGER.info("Guardando catalogo " + CatalogList.MARCA + " en BD...");
      for (Object obj : marcas) {
        Marca marca = (Marca) obj;
        marcaRepository.save(marca);
      }
      LOGGER.info("...OK");

      List<Object> transporteType = getEntitiesFromCatalogExt(CatalogList.TIPO_TRANSPORTE,
          TipoTransporte.class);
      LOGGER.info("Guardando catalogo " + CatalogList.TIPO_TRANSPORTE + " en BD...");
      for (Object obj : transporteType) {
        TipoTransporte transportType = (TipoTransporte) obj;
        transportTypeRepository.save(transportType);
      }
      LOGGER.info("...OK");

      List<Object> vehiculeUse = getEntitiesFromCatalogExt(CatalogList.USO_DEL_VEHICULO,
          UsoVehiculo.class);
      LOGGER.info("Guardando catalogo " + CatalogList.USO_DEL_VEHICULO + " en BD...");
      for (Object obj : vehiculeUse) {
        UsoVehiculo transportType = (UsoVehiculo) obj;
        vehicleUseRepository.save(transportType);
      }
      LocalTime end = LocalTime.now();
      LOGGER.info("...OK");
      LOGGER.info("Time: " + LocalTime.ofSecondOfDay(Duration.between(init, end).getSeconds()));
    } catch (Exception excep) {
      LOGGER.info("ERROR al guardar datos a BD... ");
      LOGGER.info(excep.getMessage());
      throw new HandlerException(CodeErrorClient.ERROR_SAVING_DATABASE,
          "Error al actualizar catalogo en la base de datos");
    }
  }
}
