package mx.com.amis.catalogclient.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.com.amis.catalogclient.entities.Cobertura;

public interface CoverageRepository extends JpaRepository<Cobertura, Long> {

}
