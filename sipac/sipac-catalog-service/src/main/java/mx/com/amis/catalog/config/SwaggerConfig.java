package mx.com.amis.catalog.config;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;

import io.swagger.annotations.Api;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author IDS Comercial - manolasco
 * 
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

  @Bean
  public Docket sadrApi() {
    return new Docket(DocumentationType.SWAGGER_2).groupName("mx.com.amis").apiInfo(apiInfo())
        .select().apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
        .paths(PathSelectors.any()).build().pathMapping("/")
        .genericModelSubstitutes(ResponseEntity.class)
        .directModelSubstitute(LocalDate.class, ArrayList.class)
        .directModelSubstitute(LocalTime.class, ArrayList.class).useDefaultResponseMessages(false);
  }

  private ApiInfo apiInfo() {
    final String description = "Lista de nombres de catalogos:\r\n" + "[TIPO_AUTO, \r\n"
        + "MARCA, \r\n" + "TIPO_TRANSPORTE, \r\n" + "153-USO_DEL_VEHICULO]";

    return new ApiInfoBuilder().title("Catalogs").description(description).termsOfServiceUrl("")
        .license("").licenseUrl("").version("1.0").build();
  }
}
