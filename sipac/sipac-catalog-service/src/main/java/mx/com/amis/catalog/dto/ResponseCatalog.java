package mx.com.amis.catalog.dto;

import java.util.ArrayList;
import java.util.List;

public class ResponseCatalog {
  private List<CatalogTable> catalogTable;

  public ResponseCatalog() {
    catalogTable = new ArrayList<>();
  }

  public List<CatalogTable> getCatalogTable() {
    return catalogTable;
  }

  public void setCatalogTable(List<CatalogTable> catalogTable) {
    this.catalogTable = catalogTable;
  }
}
