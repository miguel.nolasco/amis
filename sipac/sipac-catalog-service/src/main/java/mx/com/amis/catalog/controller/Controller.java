package mx.com.amis.catalog.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import mx.com.amis.catalog.dto.ResponseError;
import mx.com.amis.catalog.services.CatalogService;
import mx.com.amis.catalogclient.constants.CodeErrorClient;

@CrossOrigin
@RestController
@Api
public class Controller {
  private static final Logger LOGGER = LoggerFactory.getLogger(Controller.class);
  @Autowired
  private CatalogService catalogService;

  /**
   * Obtiene catalogo desde servicio REST de catalogos.
   * 
   * @param catalogName
   *          nombre del catalogo deseado
   * @return
   */
  @GetMapping("getUpdatedCatalog/{catalogName}")
  public ResponseEntity<Object> getUpdatedCatalog(@PathVariable final String catalogName) {
    Object response = null;
    HttpStatus httpStatus = HttpStatus.ACCEPTED;
    try {
      LOGGER.info("Obteniendo catalogo...");
      response = catalogService.getCatalog(catalogName);
      LOGGER.info("...OK");
    } catch (Exception excep) {
      httpStatus = HttpStatus.BAD_REQUEST;
      response = new ResponseError(CodeErrorClient.NONEXISTENT_CATALOG, excep.toString());
      LOGGER.info("...ERROR");
    }
    return new ResponseEntity<Object>(response, httpStatus);
  }

  /**
   * Obtiene catalogo en base de datos.
   * 
   * @param catalogName
   *          Nombre del catalogo deseado
   * @return
   */
  @GetMapping("getCatalogDataBase/{catalogName}")
  public ResponseEntity<Object> getCatalogDataBase(@PathVariable final String catalogName) {
    Object response = null;
    HttpStatus httpStatus = HttpStatus.ACCEPTED;
    try {
      LOGGER.info("Obteniendo catalogo...");
      response = catalogService.getCatalogDb(catalogName);
      LOGGER.info("...OK");
    } catch (Exception excep) {
      httpStatus = HttpStatus.BAD_REQUEST;
      response = new ResponseError(CodeErrorClient.NONEXISTENT_CATALOG, excep.getMessage());
      LOGGER.info("...ERROR");
    }
    return new ResponseEntity<Object>(response, httpStatus);
  }
}
