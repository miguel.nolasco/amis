package mx.com.amis.catalog.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import mx.com.amis.catalog.dto.CatalogTable;
import mx.com.amis.catalog.dto.ResponseCatalog;
import mx.com.amis.catalog.exceptions.HandlerException;
import mx.com.amis.catalogclient.clients.CatalogClient;
import mx.com.amis.catalogclient.constants.CatalogList;
import mx.com.amis.catalogclient.constants.CodeErrorClient;
import mx.com.amis.catalogclient.dto.SimpleCatalogValues;
import mx.com.amis.catalogclient.repositories.CircumstancesRepository;
import mx.com.amis.catalogclient.repositories.ColorRepository;
import mx.com.amis.catalogclient.repositories.CompanyRepository;
import mx.com.amis.catalogclient.repositories.CoverageRepository;
import mx.com.amis.catalogclient.repositories.InjuredTypeRepository;
import mx.com.amis.catalogclient.repositories.InjuryRepository;
import mx.com.amis.catalogclient.repositories.MarkRepository;
import mx.com.amis.catalogclient.repositories.MunicipalityRepository;
import mx.com.amis.catalogclient.repositories.OrderTypeRepository;
import mx.com.amis.catalogclient.repositories.OriginRepository;
import mx.com.amis.catalogclient.repositories.PersonTypeRepository;
import mx.com.amis.catalogclient.repositories.StateRepository;
import mx.com.amis.catalogclient.repositories.TransportTypeRepository;
import mx.com.amis.catalogclient.repositories.VehicleUseRepository;

@Service
public class CatalogService {
  private final static Logger LOGGER = LoggerFactory.getLogger(CatalogService.class);

  @Autowired
  private MarkRepository markRepository;

  @Autowired
  private OrderTypeRepository orderTypeRepository;

  @Autowired
  private TransportTypeRepository transportTypeRepository;

  @Autowired
  private VehicleUseRepository vehicleUseRepository;

  @Autowired
  private OriginRepository originRepository;

  @Autowired
  private InjuryRepository injuryReposirtory;

  @Autowired
  private InjuredTypeRepository injuredTypeRepository;

  @Autowired
  private CompanyRepository companyRepository;

  @Autowired
  private ColorRepository colorRepository;

  @Autowired
  private MunicipalityRepository municipalityRepository;

  @Autowired
  private StateRepository stateRepository;

  @Autowired
  private CircumstancesRepository circumstancesRepository;

  @Autowired
  private CoverageRepository coverageRepository;

  @Autowired
  private PersonTypeRepository personTypeRepository;

  @Autowired
  private CatalogClient catalogClient;

  /**
   * Obtiene los valores del catalogo deseado desde el servicio REST.
   * 
   * @param catalogName
   *          Nombre del catalogo
   * @return
   * @throws mx.com.amis.catalogclient.exceptions.HandlerException
   *           handlerException
   */
  public ResponseCatalog getCatalog(final String catalogName)
      throws mx.com.amis.catalogclient.exceptions.HandlerException {
    ResponseCatalog response = new ResponseCatalog();

    List<SimpleCatalogValues> simpleCatalogValues = catalogClient.getCatalogValues(catalogName);
    for (SimpleCatalogValues value : simpleCatalogValues) {
      CatalogTable register = new CatalogTable();
      register.setId(value.getId1());
      register.setDescription(value.getValue1());
      response.getCatalogTable().add(register);
    }
    return response;
  }

  /**
   * Regresa los valores del catalogo deseado alojado en base de datos.
   * 
   * @param catalogName
   *          Nombre del catalogo a consultar.
   * @return
   * @throws HandlerException
   */
  @Cacheable(value = "catalogCache", key = "#catalogName", unless = "#result==null")
  public Object getCatalogDb(final String catalogName) throws HandlerException {
    Object catalog = null;

    try {
      switch (catalogName) {
      case CatalogList.MARCA:
        catalog = markRepository.findAll();
        break;
      case CatalogList.TIPO_TRANSPORTE:
        catalog = transportTypeRepository.findAll();
        break;
      case CatalogList.USO_DEL_VEHICULO:
        catalog = vehicleUseRepository.findAll();
        break;
      case CatalogList.TIPO_ORDEN:
        catalog = orderTypeRepository.findAll();
        break;
      case CatalogList.ORIGEN:
        catalog = originRepository.findAll();
        break;
      case CatalogList.TIPO_PERSONA:
        catalog = personTypeRepository.findAll();
        break;
      case CatalogList.LESION:
        catalog = injuryReposirtory.findAll();
        break;
      case CatalogList.TIPO_LESIONADO:
        catalog = injuredTypeRepository.findAll();
        break;
      case CatalogList.COMPANIA:
        catalog = companyRepository.findAll();
        break;
      case CatalogList.COLOR:
        catalog = colorRepository.findAll();
        break;
      case CatalogList.MUNICIPIO:
        catalog = municipalityRepository.findAll();
        break;
      case CatalogList.ESTADO:
        catalog = stateRepository.findAll();
        break;
      case CatalogList.CIRCUNSTANCIA:
        catalog = circumstancesRepository.findAll();
        break;
      case CatalogList.COBERTURA:
        catalog = coverageRepository.findAll();
      default:
        throw new HandlerException(CodeErrorClient.NONEXISTENT_CATALOG, "No existe ese catalogo");
      }

    } catch (NullPointerException excep) {
      throw new HandlerException(CodeErrorClient.NONEXISTENT_CATALOG, "No existe ese catalogo");
    } catch (Exception excep) {
      throw new HandlerException(CodeErrorClient.ERROR_EXTRACTING_CATALOG, excep.getMessage());
    }
    return catalog;
  }
}
