package mx.com.amis.catalog.services;

import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import mx.com.amis.catalogclient.clients.CatalogClient;
import mx.com.amis.catalogclient.exceptions.HandlerException;

@Component
public class ScheduledTasks {

  private static final Logger LOGGER = LoggerFactory.getLogger(ScheduledTasks.class);

  @Autowired
  private CatalogClient catalogClient;

  /**
   * Metodo que se ejecuta cada X tiempo, para actualizar los catalogos en base de
   * datos.
   * 
   * @throws ServiceException
   *           serviceException
   * @throws HandlerException
   *           handlerException
   */
  @Scheduled(cron = "0 0 1 * * ? ")
  public void updateCatalogDb() throws ServiceException, HandlerException {
    LOGGER.info("Actualizado catalogos...");
    catalogClient.updateCatalogs();
    LOGGER.info("...OK");
  }
}
