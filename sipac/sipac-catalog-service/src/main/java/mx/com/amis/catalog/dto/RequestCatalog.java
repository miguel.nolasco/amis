package mx.com.amis.catalog.dto;

public class RequestCatalog {
  private String catalogName;

  public String getCatalogName() {
    return catalogName;
  }

  public void setCatalogName(String catalogName) {
    this.catalogName = catalogName;
  }
}
