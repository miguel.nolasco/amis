package mx.com.amis.catalog.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@ComponentScan({ "mx.com.amis", "mx.com.amis.catalogclient.constants", "mx.com.amis.catalog",
    "mx.com.amis.catalogclient.repositories", "mx.com.amis.catalog.services" })
@EnableJpaRepositories({ "mx.com.amis.catalogclient.repositories" })
@EntityScan({ "mx.com.amis.catalogclient.entities" })
@EnableScheduling
@EnableCaching
public class Application {
  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }
}
